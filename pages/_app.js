import "../styles/globals.css"
import { useRouter } from "next/router"
import { NextUIProvider } from "@nextui-org/react"
import { ThemeProvider as NextThemesProvider } from "next-themes"
import { darkTheme, lightTheme } from "@libs/themes"
import NextNProgress from "nextjs-progressbar"
import Seo from "@components/seo"

const MyApp = ({ Component, pageProps }) => {

  const router = useRouter()

  return (
    <NextThemesProvider
      defaultTheme="system"
      attribute="class"
      value={{
        light: lightTheme.className,
        dark: darkTheme.className
      }}>
        <Seo />
      <NextUIProvider>
        <NextNProgress
          color="primary"
          options={{ showSpinner: false, easing: "ease" }}
        />
          <Component {...pageProps} key={router.route}/>
      </NextUIProvider>
    </NextThemesProvider>
  )
}

export default MyApp
