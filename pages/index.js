import {
  Card,
  Container, 
  Grid,
  Text,
} from "@nextui-org/react"
import Seo from "@components/seo"
import { seo } from "@data/pages/home"
import Navbar from "@components/navbar"
import Footer from "@components/footer"
import {
  Hero, Product, Partnership, Career, Testimoni, Activity
} from "@components/layouts/home"
import WhatsApp from "@components/whatsapp"

export default function Home() {
  return (
    <>
      <Seo
        isHomepage={true}
        title={seo.title}
        description={seo.description}
        siteName={seo.siteName}
        canonical={seo.canonical}
      />
      <Navbar />
      
      <Hero />

      <Product />

      <Partnership />

      <Career />

      <Testimoni />

      <Activity />
      
      <Footer />

      <WhatsApp />

    </>
  )
}
