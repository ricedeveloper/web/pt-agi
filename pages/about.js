import Navbar from "@components/navbar"
import Footer from "@components/footer"
import {
    Container, Text, Card, Grid
} from "@nextui-org/react"
import Seo from "@components/seo"
import { seo, about, visimisi, legalitas } from "@data/pages/about"

const PageAbout = () => {
    return (
        <>
            <Seo
                title={seo.title}
                description={seo.description}
                siteName={seo.siteName}
                canonical={seo.canonical}
            />
            <Navbar />
            <Container responsive
                css={{
                    minHeight: "100vh",
                    py: "$20",
                }}
            >
                <Text h2 weight="black">{ about.title }</Text>
                { about.description.map((item, index) => (
                    <Text size={20} key={index} css={{ my: "$10"}}>{ item }</Text>
                ))}

                <Text h2 weight="black" css={{ mt: "$20"}}>Visi</Text>
                <ol>
                    { visimisi.visi.map((item, index) => (
                        <li key={index}>
                            <Text size={20} css={{ my: "$4"}}>{ item }</Text>
                        </li>
                    ))}
                </ol>

                <Text h2 weight="black" css={{ mt: "$20"}}>Misi</Text>
                <ol>
                    { visimisi.misi.map((item, index) => (
                        <li key={index}>
                            <Text size={20} css={{ my: "$4"}}>{ item }</Text>
                        </li>
                    ))}
                </ol>

                <Text h2 weight="black" css={{ mt: "$20"}}>Legalitas Perusahaan</Text>
                <Grid.Container gap={4} css={{ my: "$4"}}>
                    { legalitas.map((item, index) => (
                        <Grid xs={12} sm={6} key={index}>
                            <Card cover>
                                <Card.Image src={item} />
                            </Card>
                        </Grid>
                    ))}
                </Grid.Container>

            </Container>
            <Footer />
        </>
    )
}

export default PageAbout