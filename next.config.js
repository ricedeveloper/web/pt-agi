/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: [
      "localhost", "ptagi.co.id"
    ]
  }
}

module.exports = nextConfig
