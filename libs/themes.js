import { createTheme } from "@nextui-org/react"

const fonts = {
    sans: 'Inter, -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif',
    mono: 'Menlo, Monaco, Lucida Console, Liberation Mono, DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace'
}

export const darkTheme = createTheme({
  type: 'dark',
  theme: {
    colors: {
      primaryLight: '$green200',
      primaryLightHover: '$green300',
      primaryLightActive: '$green400',
      primaryLightContrast: '$green600',
      primary: '$green500',
      primaryBorder: '$green500',
      primaryBorderHover: '$green600',
      primarySolidHover: '$green700',
      primarySolidContrast: '$white',
      primaryShadow: '$green500',

      secondaryLight: '$yellow200',
      secondaryLightHover: '$yellow300',
      secondaryLightActive: '$yellow400',
      secondaryLightContrast: '$yellow600',
      secondary: '$yellow500',
      secondaryBorder: '$yellow500',
      secondaryBorderHover: '$yellow600',
      secondarySolidHover: '$yellow700',
      secondarySolidContrast: '$white',
      secondaryShadow: '$yellow500',
  
      gradient: 'linear-gradient(112deg, $blue100 -25%, $yellow500 -10%, $green500 80%)',
      link: '#5E1DAD',
      blockquote: '#1F1F1F',
      textColorFooter: '#F1F1F1',
      bgFooter: '$gray100',
      bgCopyright: '$gray50'
    },
    fonts,
    shadows: {
      xs: '-4px 0 4px rgb(0 0 0 / 5%);',
      sm: '0 5px 20px -5px rgba(0, 0, 0, 0.1)',
      md: '0 8px 30px rgba(0, 0, 0, 0.15)',
      lg: '0 30px 60px rgba(0, 0, 0, 0.15)',
      xl: '0 40px 80px rgba(0, 0, 0, 0.25)'
    }
  }
})

export const lightTheme = createTheme({
    type: 'light',
    theme: {
      colors: {
        primaryLight: '$green200',
        primaryLightHover: '$green300',
        primaryLightActive: '$green400',
        primaryLightContrast: '$green600',
        primary: '$green500',
        primaryBorder: '$green500',
        primaryBorderHover: '$green600',
        primarySolidHover: '$green700',
        primarySolidContrast: '$black',
        primaryShadow: '$green500',

        secondaryLight: '$yellow200',
        secondaryLightHover: '$yellow300',
        secondaryLightActive: '$yellow400',
        secondaryLightContrast: '$yellow600',
        secondary: '$yellow500',
        secondaryBorder: '$yellow500',
        secondaryBorderHover: '$yellow600',
        secondarySolidHover: '$yellow700',
        secondarySolidContrast: '$black',
        secondaryShadow: '$yellow500',
    
        gradient: 'linear-gradient(112deg, $blue100 -25%, $yellow500 -10%, $green500 80%)',
        link: '#5E1DAD',
        blockquote: '#1F1F1F',
        textColorFooter: '#F1F1F1',
        bgFooter: '$gray50',
        bgCopyright: '$gray100'
      },
      fonts
    }
})