export const VisiMisi = {
    visi: [
        "Menjadi pengembang teknologi pertanian terkemuka di Indonesia guna mendukung pertanian nasional yang tangguh dan berkelanjutan"
    ],
    misi: [
        "Menghasilkan produk \"Pupuk organik Cair\" yang berbasis pada teknologi APD-19",
        "Memproduksi \"Pupuk Organik Cair\" pada beberapa wilayah agar tersebar guna memenuhi kebutuhan produk-produk Pertanian guna meningkatkan produktivitas dan kualitas produk",
        "Mengembang teknologi pertanian, secara terus menerus dengan memperhatikan aspek kemampuan dan kompetensi swausaha yang berwawasan lingkungan.",
        "Memberikan produk-produk yang bermanfaat dan optimal demi meningkatkan kesejahteraan masyarakat Pertanian, Peternakan dan Perikanan serta memperbaiki lingkungan hidup."
    ]
}