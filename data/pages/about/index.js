export { Seo as seo} from "./seo"
export { About as about } from "./about"
export { VisiMisi as visimisi} from "./visi-misi"
export { Legalitas as legalitas } from "./legalistas"