export const Services = [
    {
        title: "💹 Support Pemasaran",
        description: "Dengan menjadi mitra atau distributor resmi akan mendapatkan support Product seperti kaos petani, brosur, banner dan buku Aplikasi."
    },
    {
        title: "📦 Pengiriman Seluruh Indonesia",
        description: "Semua produk PT. AGI dapat dikirim keseluruh Indonesia dengan cepat dan aman serta bergaransi."
    },
    {
        title: "💅 Konsultasi",
        description: "Dengan membeli produk, Customer akan mendapatkan pendampingan dan konsultasi secara online dari Team PT. AGI."
    },
    {
        title: "💗 Hak Eksklusif Mitra",
        description: "Mitra memiliki hak Eksklusif dari Perusahaan seperti penjualan produk brand Kilat di tingkat wilayah Desa atau Kecamatan."
    },
    {
        title: "😉 Hak Eksklusif Distributor",
        description: "Distributor memiliki hak Ekslusif penjualan produk brand Kilat di tingkat Kabupaten atau Kota."
    },
    {
        title: "💵 Subsidi Ongkir",
        description: "Untuk Mitra dan Distributor akan mendapatkan subsidi ongkir, bahkan bisa FREE Ongkir sesuai syarat dan kesepakatan dari Perusahaan"
    }
]