export { Seo as seo } from "./seo"
export { Hero as hero } from "./hero"
export { Product as product } from "./product"
export { Services as services } from "./services"
export { Partnership as partnership } from "./partnership"
export { Career as career } from "./career"
export { Testimoni as testimoni } from "./testimoni"
export { Activity as activity } from "./activity"