export const Seo = {
    title: "PT. Agrikultur Gemilang Indonesia",
    description: "PT. Agrikultur Gemilang Indonesia bergerak dibidang pemasaran produk brand KILAT untuk pertanian, perikanan dan peternakan berbasis organik",
    siteName: "PT. Agrikultur Gemilang Indonesia",
    canonical: "https://ptagi.co.id/",
    ogImage: "OG_IMAGE_URL",
    ogType: "website",
    twitter: "USERNAME_TWITTER"
}