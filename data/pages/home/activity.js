export const Activity = {
    title: "Aktivitas & Kegiatan Kami",
    description: "Berikut aktivitas serta kegiatan yang sudah kami lakukan.",
    data: [
        {
            title: "Judul",
            image: "/home/aktivitas/1.png"
        },
        {
            title: "Judul",
            image: "/home/aktivitas/2.png"
        },
        {
            title: "Judul",
            image: "/home/aktivitas/3.png"
        },
        {
            title: "Judul",
            image: "/home/aktivitas/4.png"
        },
    ]
}