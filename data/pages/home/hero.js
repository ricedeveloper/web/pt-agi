export const Hero = {
    title: "Sinergi Bersama Petani untuk Meningkatkan Kedaulatan Pangan",
    description: "Merupakan perusahaan yang bergerak pada distribusi pupuk pertanian, perikanan dan peternakan yang berbahan organik yang memiliki visi menjadi pengembang teknologi pertanian terkemuka di Indonesia guna mendukung pertanian nasional yang tangguh dan berkelanjutan.",
    button: {
        title: "Selengkapnya...",
        href: "/about"
    }
}