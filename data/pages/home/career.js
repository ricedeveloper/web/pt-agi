export const Career = {
    title: "Karir",
    description: "Kami menyambut anda yang bertalenta dan memiliki latar belakang professional untuk bertumbuh bersama kami. Komitmen kami adalah berkembang dan berinovasi bersama.",
    button: {
        title: "Yuk Lihat Peluang Karirmu",
        href: "/career"
    }
}