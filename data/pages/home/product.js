export const Product = {
    title: "Produk Kami",
    description: "Berikut ini produk unggulan kami",
    button: {
        title: "Lihat Semua Produk",
        href: "/product"
    },
    data: [
        {
            name: "Pupuk Kilat",
            price: "Rp50.000",
            image: "/product/1.png"
        },
        {
            name: "Nutrisi Ternak",
            price: "Rp50.000",
            image: "/product/1.png"
        },
        {
            name: "Nutrisi Ikan",
            price: "Rp50.000",
            image: "/product/1.png"
        },
        
    ]
}