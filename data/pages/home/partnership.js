export const Partnership = {
    title : "Kemitraan",
    description: "PT Agrikultur Gemilang Indonesia menjalin Kerjasama dengan pihak perorangan, badan usaha swasta atau pemerintahan untuk menjadi rekan mitra kerja dalam distribusi produk, dan saat ini kami telah memiliki rekan mitra dan distributor dibeberapa wilayah.",
    videoId : "vyocN2Hy4pc",
    button: {
        title: "Selengkapnya...",
        href: "/partnership"
    }
}