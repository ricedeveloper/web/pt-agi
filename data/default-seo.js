export const DefaultSeo = {
    favicon: '/favicon.ico',
    title: 'PT. Agrikultur Gemilang Indonesia',
    description: 'PT. Agrikultur Gemilang Indonesia bergerak dibidang pemasaran produk brand KILAT untuk pertanian, perikanan dan peternakan berbasis organik',
    siteName: 'PT. Agrikultur Gemilang Indonesia',
    canonical: 'https://ptagi.co.id/',
    ogImage: 'https://storage.googleapis.com/brandflow-bucket/personal/blog/portfolio-og.jpg',
    ogType: 'website',
    twitter: 'USERNAME_TWITTER'
}