export const MenuData = [
    {
        title: "Produk",
        href: "/#product",
        isNew: false,
        openNewTab: false
    },
    {
        title: "Kemitraan",
        href: "/#partnership",
        isNew: false,
        openNewTab: false
    },
    {
        title: "Karir",
        href: "/#career",
        isNew: false,
        openNewTab: false
    },
    {
        title: "Testimoni",
        href: "/#testimoni",
        isNew: false,
        openNewTab: false
    },
    {
        title: "Aktivitas",
        href: "/#activities",
        isNew: false,
        openNewTab: false
    },
    {
        title: "About",
        href: "/about",
        isNew: false,
        openNewTab: false
    },
    
]