export { About as about } from "./about"
export { Contact as contact } from "./contact"
export { Menu as menu } from "./menu"
export { Product as product } from "./product"