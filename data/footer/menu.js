export const Menu = {
    title: "Menu",
    data: [
        {
            title: "Tentang Kami",
            href: "/about"
        },
        {
            title: "Produk Kami",
            href: "/product"
        },
        {
            title: "Pemesanan",
            href: "/order"
        },
    ]
}