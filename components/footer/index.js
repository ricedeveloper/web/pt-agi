import {
    Container, Grid, Col, Row, Text, Link
} from "@nextui-org/react"
import { about, contact, menu, product } from "@data/footer"

const Footer = () => {
    return (
        <footer>
            <Container fluid
                css={{
                    px: "$0"
                }}
            >
                <Col 
                    css={{
                        px: "$8",
                        bg: "$bgFooter"
                    }}
                >
                    <Grid.Container gap={4} justify="space-between">
                        <Grid xs={12} sm={3}>
                            <Col>    
                                <Text h3 weight="black">{about.title}</Text>
                                <Text>{about.description}</Text>
                            </Col>
                        </Grid>
                        <Grid xs={12} sm={3}>
                            <Col>
                                <Text h3>{menu.title}</Text>
                                { menu.data.map((item, index) => (
                                    <Link href={item.href} key={index}
                                        color="text"
                                        css={{
                                            display: "block",
                                            py: "$2"
                                        }}
                                    >{item.title}</Link>
                                ))}
                            </Col>
                        </Grid>
                        <Grid xs={12} sm={3}>
                            <Col>
                                <Text h3>{product.title}</Text>
                                { product.data.map((item, index) => (
                                    <Link href={item.href} key={index} 
                                        color="text"
                                        css={{
                                            display: "block",
                                            py: "$2"
                                        }}
                                    >{item.title}</Link>
                                ))}
                            </Col>
                        </Grid>
                        <Grid xs={12} sm={3}>
                            <Col>
                                <Text h3>{contact.title}</Text>
                                <Text>{contact.address}</Text>
                                <Text css={{mt: "$6"}}>☎ {contact.phone}</Text>
                                <Text>✉ {contact.email}</Text>
                            </Col>
                        </Grid>
                    </Grid.Container>
                </Col>
            </Container>
            <Col css={{
                bg: "$bgCopyright"
            }}>
                <Text css={{ p: "$4", textAlign: "center"}}>&copy; {` PT. Agrikultur Gemilang Indonesia`}</Text>
            </Col>
        </footer>
    )
}

export default Footer