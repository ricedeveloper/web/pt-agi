import NextLink from 'next/link'
import Image from 'next/image'
import { useTheme, Link, Text } from '@nextui-org/react'
import { DefaultSeo } from '@data/default-seo'

export default function Logo(props){
    const { title } = DefaultSeo
    const { isDark } = useTheme()
    const logoPath = `/logo/1agi${isDark ? '-dark.png' : '.png'}`
    return (
        <NextLink href="/" passHref>
            <Link color="text" css={{ display: 'flex', alignItems: 'center', gap: 4}}>
                <Image src={logoPath} alt={title} width={60} height={60} objectFit='scale-down' />
                {/* <Text
                    as="h2"
                    size={18}
                    weight="bold"
                    ml={2}
                    css={{
                        display: 'none',
                        '@xsMax': {
                            display: 'none'
                        },
                        '@sm': {
                            display: 'block'
                        },
                        '@md': {
                            display: 'block'
                        },
                        '@lg': {
                            display: 'block'
                        }
                    }}
                    >
                    {title}
                </Text> */}
            </Link>
        </NextLink>
    )
}