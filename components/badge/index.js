import { Text, useTheme } from "@nextui-org/react"

const Badge = ({ children }) => {
    const { isDark } = useTheme()
    return (
        <Text
            css={{
                textTransform: 'uppercase',
                padding: '5px 5px',
                margin: '0 2px',
                fontSize: '10px',
                fontWeight: '800',
                borderRadius: '14px',
                letterSpacing: '0.6px',
                lineHeight: 1,
                textShadow: '0 1px 1px rgba(0, 0, 0, 0.16)',
                boxShadow: '1px 2px 5px 0px rgb(0 0 0 / 10%)',
                alignItems: 'center',
                alignSelf: 'center',
                color: isDark ? '$black' : '$white',
                bg: '$secondary'
            }}
        >
            { children }
        </Text>
    )
}

export default Badge