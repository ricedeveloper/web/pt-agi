import Head from 'next/head'
import { DefaultSeo } from "@data/default-seo"

const DEFAULT_FAVICON = "/favicon.ico"

export default function Seo({
  isHomepage = false,
  title = DefaultSeo.title,
  description = DefaultSeo.description,
  siteName = DefaultSeo.siteName,
  canonical = DefaultSeo.canonical,
  ogImage = DefaultSeo.ogImage,
  ogType = DefaultSeo.ogType,
  twitterHandle = "@jabirdeveloper",
  favicon = DefaultSeo.favicon
}) {
  return (
    <Head>
      { isHomepage ? (
        <title key="title">{siteName}</title>
      ):
        <title key="title">{`${title} \u2014 ${siteName}`}</title>
      }
      <meta name="description" content={description} />
      <meta key="og_type" property="og:type" content={ogType} />
      <meta key="og_title" property="og:title" content={title} />
      <meta key="og_description" property="og:description" content={description} />
      <meta key="og_locale" property="og:locale" content="en_IE" />
      <meta key="og_site_name" property="og:site_name" content={siteName} />
      <meta key="og_url" property="og:url" content={canonical ?? DefaultSeo.canonical} />
      <meta key="og_site_name" property="og:site_name" content={siteName} />
      <meta
        key="og_image"
        property="og:image"
        content={ogImage ?? DefaultSeo.ogImage}
      />
      <meta
        key="og_image:alt"
        property="og:image:alt"
        content={`${title} | ${siteName}`}
      />
      <meta key="og_image:width" property="og:image:width" content="1200" />
      <meta key="og_image:height" property="og:image:height" content="630" />

      <meta name="robots" content="index,follow" />

      <meta
        key="twitter:card"
        name="twitter:card"
        content="summary_large_image"
      />
      <meta 
        key="twitter:site" 
        name="twitter:site" 
        content={twitterHandle} 
      />
      <meta
        key="twitter:creator"
        name="twitter:creator"
        content={twitterHandle}
      />
      <meta 
        key="twitter:title" 
        property="twitter:title" 
        content={title} 
      />
      <meta
        key="twitter:description"
        property="twitter:description"
        content={description}
      />

      <link rel="canonical" href={canonical ?? DefaultSeo.canonical} />

      <link rel="shortcut icon" href={favicon ?? DefaultSeo.favicon} />
    </Head>
  )
}