import { useRef } from "react"
import { useRouter } from "next/router"
import {
    Container, Popover, Image, Text, Button, Grid, Row, Textarea, Spacer,
} from "@nextui-org/react"

const WhatsApp = () => {
    const phone = "6285157426002"
    let url = "https://wa.me/" + phone + "?lang=en&text="

    const router = useRouter()
    const messageRef = useRef(null)
    const sendMessage = () => {
        if(messageRef.current){
            url = url + encodeURIComponent(messageRef.current.value)
            router.push(url)
        }
    }

    return(
        <Container fluid
            css={{
                w: "max-content",
                h: "max-content",
                position: "fixed",
                bottom: "$10",
                right: "$10"
            }}
        >
            <Popover placement="top-right">
                <Popover.Trigger>
                    <Image src="/brand/whatsapp.svg" height={60} width={60} alt="WhatsApp" css={{"@hover": { cursor: "pointer"}}}/>
                </Popover.Trigger>
                <Popover.Content>
                    <Grid.Container
                        css={{ borderRadius: "14px", padding: "1rem", maxWidth: "330px" }}
                        >
                        <Row justify="center" align="center">
                            <Text b>Pesan</Text>
                        </Row>
                        <Row>
                            <Textarea ref={messageRef} placeholder="Tulis pesan..." rows={4} width="100%" css={{ my: "$8", bg: "$accents2"}}/>
                        </Row>
                        <Grid.Container justify="flex-end" alignContent="center">
                            <Grid>
                                <Button size="sm" color="success"
                                    onPress={sendMessage}
                                >
                                    Kirim
                                </Button>
                            </Grid>
                        </Grid.Container>
                    </Grid.Container>
                </Popover.Content>
            </Popover>
        </Container>
    )
}

export default WhatsApp