import { useState } from 'react'
import { Container, Grid } from '@nextui-org/react'
import Logo from '@components/logo'
import ToggleMenu from './toggle-menu'
import MenuItems from './menu-items'

const NavbarContainer = ({ children, ...props }) => {
    return (
        <Grid.Container
            as='navbar'
            css={{
                boxShadow: '$xs',
                position: 'sticky',
                top: 0,
                bg: '$background',
                zIndex: "$max"
            }}
            {...props}
        >
            <Container
                css={{
                    display: 'flex',
                    flexWrap: 'wrap',
                    gap: '$4',
                    p: '$4',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignContent: 'center',
                    alignItems: 'center',
                    '@lg': {
                        p: '$4'
                    }
                }}
            >
                {children}
            </Container>
        </Grid.Container>
    )
}

const Navbar = (props) => {
    const [isOpen, setIsOpen] = useState(false)
  
    const toggle = () => setIsOpen(!isOpen)

    return (
        <NavbarContainer {...props}>
            <Logo />
            <ToggleMenu toggle={toggle} isOpen={isOpen}/>
            <MenuItems isOpen={isOpen}/>

        </NavbarContainer>
    )
}

export default Navbar