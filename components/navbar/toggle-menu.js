import { Button } from "@nextui-org/react"
import { RiMenu3Fill, RiCloseFill } from "react-icons/ri"

const ToggleMenu = ({ toggle, isOpen }) => {
    return (
        <Button
            auto
            light
            css={{
                pl: "unset",
                pr: "unset",
                "@xs": {
                    display: "block",
                },
                "@sm": {
                    display: "none"
                }
            }}
            onClick={toggle}
            icon={isOpen ? <RiCloseFill/> : <RiMenu3Fill  />}
        />
    )
}

export default ToggleMenu