import { useRouter } from "next/router"
import NextLink from "next/link"

import { Button, Container, Link, useTheme, Spacer } from "@nextui-org/react"
import SwitchTheme from "@components/utils/changeTheme"
import Badge from "@components/badge"
import { MenuData } from "@data/navbar"

const MenuLink = ({ item, isLast }) => {
    const router = useRouter()
    const { href, title, isNew, openNewTab } = item
        return (
            <>
                <NextLink href={href} passHref scroll={false}>
                    <Link
                        icon={openNewTab}
                        color="text"
                        target={openNewTab ? "_blank" : "_parent"}
                        css={{
                            textTransform: "uppercase",
                            fontWeight: "$medium"
                        }}
                    >
                        { title }
                        { isNew && (
                            <Badge variant="primary">New</Badge>
                        )}
                    </Link>
                </NextLink>
                {isLast && (
                    <><Spacer /><SwitchTheme /></>
                )}
            </>
        )
}

const MenuItems = ({ isOpen }) => {
    return (
        <Container
            css={{
                ml: "unset",
                mr: "unset",
                display: isOpen ? "flex" : "none",
                flexDirection: "column",
                flexWrap: "wrap",
                gap: "$10",
                alignContent: "center",
                alignItems: "center",
                "@sm": {
                    display: "flex",
                    flexDirection: "row",
                    width: "unset"
                }
            }}
        >
            {
                MenuData.map((item, index) => (
                    <MenuLink item={item} isLast={index === MenuData.length -1} key={index}/>
                ))
            }
            {/* <AvatarMenu /> */}
        </Container>
    )
}

export default MenuItems