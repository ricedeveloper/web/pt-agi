import {
    Container, Grid, Image, Text, Card
} from "@nextui-org/react"
import { activity } from "@data/pages/home"
import { useEffect } from "react"
import { motion, useAnimation } from "framer-motion"
import { useInView } from "react-intersection-observer"

const Activity = () => {
    const {ref, inView} = useInView()
    const atas = useAnimation()
    const bawah = useAnimation()
    const anim = {
        y: 0,
        opacity: 1,
        transition: {
            type: "spring", duration: 3, delay: .5
        }
    }

    useEffect(() => {
        if (inView){
            atas.start(anim)
            bawah.start(anim)
        } else {
            atas.start({ y: 10, opacity: 0})
            bawah.start({ y: -10, opacity: 0})
        }
    })

    return (
        <section id="activities" ref={ref}>
            <Container fluid
                css={{
                    minHeight: "100vh",
                    py: "$20",
                    textAlign: "center",
                }}
            >
                <motion.div animate={atas}>
                    <Text h2 weight="black">{ activity.title }</Text>
                    <Text size={20}>{ activity.description }</Text>
                </motion.div>
                <Grid.Container gap={4} justify="center"
                    css={{
                        my: "$10"
                    }}
                >
                    { activity.data.map((item, index) => (
                        <Grid xs={6} sm={3} key={index}>
                            <motion.div animate={bawah} style={{ width: "100%"}}>
                                <Card cover hoverable>
                                    <Card.Image src={item.image} />
                                </Card>
                            </motion.div>
                        </Grid>
                    ))}
                </Grid.Container>
            </Container>
        </section>
    )
}

export default Activity