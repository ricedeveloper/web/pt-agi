import {
    Container, Text, Button, Grid, Col, Link, Image, Card, Row
} from "@nextui-org/react"

import { career } from "@data/pages/home"
import { useEffect } from "react"
import { motion, useAnimation } from "framer-motion"
import { useInView } from "react-intersection-observer"

const Career = () => {
    const {ref, inView} = useInView()
    const fadeIn = useAnimation()
    const anim = {
        opacity: 1,
        transition: {
            type: "spring", duration: 3, delay: .5
        }
    }

    useEffect(() => {
        if (inView){
            fadeIn.start(anim)
        } else {
            fadeIn.start({ opacity: 0})
        }
    })
    return (
        <section id="career" ref={ref}>
            <Container fluid
                css={{
                    minHeight: "100vh",
                    px: "$0",
                    py: "$20",
                    display: "flex",
                    justifyContent: "center",
                    justifyItems: "center",
                    alignItems: "center"
                }}
            >
                <Grid.Container gap={2} justify="center">
                    <Grid xs={12} md={6}>
                        <motion.div animate={fadeIn} style={{ width: "100%"}}>
                            <Col>
                                <Text h2 weight="black" color="#F5F5F5">{ career.title }</Text>
                                <Text
                                    blockquote
                                    size={20}
                                    color="#F5F5F5"
                                    css={{
                                        bg: "$blockquote",
                                        mt: "$10"
                                    }}
                                >{ career.description }</Text>
                                <Link href={career.button.href}
                                    css={{
                                        mt: "$10"
                                    }}
                                >
                                    <Button auto rounded
                                        color="secondary"
                                    >
                                        {career.button.title}
                                    </Button>
                                </Link>
                            </Col>
                        </motion.div>
                    </Grid>
                </Grid.Container>

            </Container>
        </section>
    )
}

export default Career