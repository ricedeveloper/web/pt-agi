import { useEffect } from "react"
import {
  Card, Button, Container, Link, Grid, Text, Row, Col
} from "@nextui-org/react"
import { motion, useAnimation } from "framer-motion"
import { useInView } from "react-intersection-observer"
import { hero } from "@data/pages/home"

const Hero = () => {
    const {ref, inView} = useInView()
    const kiri = useAnimation()
    const kanan = useAnimation()
    const anim = {
        x: 0,
        opacity: 1,
        transition: {
            type: "spring", duration: 3, delay: .5
        }
    }

    useEffect(() => {
        if (inView){
            kiri.start(anim)
            kanan.start(anim)
        } else {
            kiri.start({ x: -10, opacity: 0})
            kanan.start({ x: 10, opacity: 0})
        }
    })

    return (
        <section id="hero" ref={ref}>
            <Container fluid
                css={{
                    minHeight: "100vh"
                }}
            >
                <Grid.Container gap={4}
                    css={{
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                >

                    <Grid sm={12} md={6}>
                        <motion.div animate={kiri}>
                            <Col>
                                <Text h2
                                    weight="black"
                                >
                                    {hero.title}
                                </Text>
                                <Text
                                    size={20}
                                    css={{
                                        my: "$10"
                                    }}
                                >
                                {hero.description}
                                </Text>
                                <Link href={hero.button.href}>
                                    <Button auto rounded
                                        color="secondary"
                                    >
                                        {hero.button.title}
                                    </Button>
                                </Link>
                            </Col>
                        </motion.div>
                    </Grid>
                    <Grid sm={12} md={6}>
                        <motion.div animate={kanan} style={{ width: "100%"}}>
                            <Card cover
                                css={{
                                    "@md": {
                                    mt: "$20"
                                    }
                                }}
                            >
                            <Card.Image
                                src="/home/produk-2.jpg"
                                width="100%"
                                height={360}
                                alt="Hero"
                            />
                            </Card>
                        </motion.div>
                    </Grid>
                
                </Grid.Container>
            </Container>
        </section>
    )
}

export default Hero