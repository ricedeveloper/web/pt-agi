import React from "react"
import {
    Container, Grid, Text, Col, Card
} from "@nextui-org/react"
import LiteYouTubeEmbed from "react-lite-youtube-embed"
import "react-lite-youtube-embed/dist/LiteYouTubeEmbed.css"
import { testimoni } from "@data/pages/home"
import { useEffect } from "react"
import { motion, useAnimation } from "framer-motion"
import { useInView } from "react-intersection-observer"

const Video = ({ videoId, anim }) => {
    return (
        <Grid xs={12} sm={3}>
            <motion.div animate={anim} style={{ width: "100%" }}>
                <Card cover>
                    <Card.Body >
                        <LiteYouTubeEmbed id={videoId}
                            poster="maxresdefault"
                        />
                    </Card.Body>
                </Card>
            </motion.div>
        </Grid>
    )
}

const Testimoni = () => {
    const {ref, inView} = useInView()
    const atas = useAnimation()
    const bawah = useAnimation()
    const anim = {
        y: 0,
        opacity: 1,
        transition: {
            type: "spring", duration: 3, delay: .5
        }
    }

    useEffect(() => {
        if (inView){
            atas.start(anim)
            bawah.start(anim)
        } else {
            atas.start({ y: 10, opacity: 0})
            bawah.start({ y: -10, opacity: 0})
        }
    })

    return (
        <section id="testimoni" ref={ref}>
            <Container fluid
                css={{
                    minHeight: "100vh",
                    py: "$20",
                    textAlign: "center",
                }}
            >
                <motion.div animate={atas}>
                    <Text h2 weight="black" css={{}}>{ testimoni.title }</Text>
                    <Text size={20}>{ testimoni.description }</Text>
                </motion.div>
                <Grid.Container gap={4} justify="center" css={{ my: "$10"}}>
                    {testimoni.data.map((item, index) => (
                        <Video videoId={item.videoId} anim={bawah} key={index} />
                    ))}
                </Grid.Container>
            </Container>
        </section>
    )
}

export default Testimoni