import React from "react"
import {
    Container, Text, Grid, Col, Button, Link, Card
} from "@nextui-org/react"
import LiteYouTubeEmbed from "react-lite-youtube-embed"
import "react-lite-youtube-embed/dist/LiteYouTubeEmbed.css"
import { partnership } from "@data/pages/home"
import { useEffect } from "react"
import { motion, useAnimation } from "framer-motion"
import { useInView } from "react-intersection-observer"

const Partnership = () => {
    const {ref, inView} = useInView()
    const kiri = useAnimation()
    const kanan = useAnimation()
    const anim = {
        x: 0,
        opacity: 1,
        transition: {
            type: "spring", duration: 3, delay: .5
        }
    }

    useEffect(() => {
        if (inView){
            kiri.start(anim)
            kanan.start(anim)
        } else {
            kiri.start({ x: -10, opacity: 0})
            kanan.start({ x: 10, opacity: 0})
        }
    })
    return (
        <section id="partnership" ref={ref}>
            <Container fluid
                css={{
                    pt: "$20",
                    minHeight: "100vh"
                }}
            >
                <Grid.Container gap={4} justify="center">
                    <Grid xs={12} sm={6}>
                        <motion.div animate={kiri} style={{ width: "100%"}}>
                            <Card cover >
                                <Card.Body>
                                    <LiteYouTubeEmbed id={partnership.videoId}
                                        poster="maxresdefault"
                                    />
                                </Card.Body>
                            </Card>
                        </motion.div>
                    </Grid>
                    <Grid xs={12} sm={6}>
                        <motion.div animate={kanan} style={{ width: "100%"}}>
                            <Col>
                            <Text h2 weight="black">{ partnership.title }</Text>
                            <Text size={20} css={{ my: "$10"}}>{ partnership.description }</Text>
                            <Link href={partnership.button.href}>
                                <Button auto rounded
                                    color="secondary"
                                >
                                    {partnership.button.title}
                                </Button>
                            </Link>
                            </Col>
                        </motion.div>
                    </Grid>
                </Grid.Container>
            </Container>
        </section>
    )
}

export default Partnership