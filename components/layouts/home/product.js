import {
    Container, Grid, Card, Row, Text, Link, Button
} from "@nextui-org/react"
import { product } from "@data/pages/home"
import { useEffect } from "react"
import { motion, useAnimation } from "framer-motion"
import { useInView } from "react-intersection-observer"

const ItemProduct = ({ item, anim }) => {
  return (
      <Grid xs={6} sm={3}>
        <motion.div animate={anim} style={{ width: "100%" }}>
          <Card hoverable clickable>
            <Card.Body css={{ p: 0 }}>
              <Card.Image
                objectFit="contain"
                src={item.image}
                width="100%"
                height={140}
                alt={item.name}
              />
            </Card.Body>
            <Card.Footer justify="flex-start">
              <Row wrap="wrap" justify="space-between">
                <Text b>{item.name}</Text>
                <Text css={{ color: "$primaryBorderHover", fontWeight: "$semibold" }}>
                  {item.price}
                </Text>
              </Row>
            </Card.Footer>
          </Card>
        </motion.div>
      </Grid>
  )
}

const Product = () => {
    const {ref, inView} = useInView()
    const atas = useAnimation()
    const bawah = useAnimation()
    const anim = {
      y: 0,
      opacity: 1,
      transition: {
          type: "spring", duration: 3, delay: .5
      }
    }

      useEffect(() => {
          if (inView){
              atas.start(anim)
              bawah.start(anim)
          } else {
              atas.start({ y: 10, opacity: 0})
              bawah.start({ y: -10, opacity: 0})
          }
      })
    
    return (
        <section id="product" ref={ref}>
            <Container fluid
                css={{
                    minHeight: "100vh",
                    py: "$20",
                    textAlign: "center",
                }}
            >
                <motion.div animate={atas}>
                  <Text h2 weight="black">{ product.title }</Text>
                  <Text size={20}>{ product.description }</Text>
                </motion.div>
               
                <Grid.Container
                    gap={4}
                    justify="center"
                    css={{
                        my: "$10"
                    }}
                >
                    {product.data.map((item, index) => (
                        <ItemProduct item={item} anim={bawah} key={index} />
                    ))}
                </Grid.Container>
                <motion.div animate={bawah}>
                  <Link href={product.button.href}>
                      <Button auto rounded
                          color="secondary"
                      >
                          {product.button.title}
                      </Button>
                  </Link>
                </motion.div>
            </Container>
        </section>
    )
}

export default Product